<?php

namespace TShirtAndSons\Shopify\Facades;

use Illuminate\Support\Facades\Facade;

class Shopify extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'shopify';
    }
}