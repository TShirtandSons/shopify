<?php

namespace TShirtAndSons\Shopify;

use Illuminate\Support\ServiceProvider;
use PHPShopify\ShopifySDK;

class ShopifyServiceProvider extends ServiceProvider
{
    public function boot(): void
    {

    }

    public function register(): void
    {
        $this->app->singleton('shopify', function () {
           return new ShopifySDK();
        });
    }
}